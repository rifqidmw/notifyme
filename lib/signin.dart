import 'package:notifyme/home.dart';
import 'package:notifyme/signup.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'const.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  SharedPreferences prefs;

  bool isLoading = false;
  bool isLoggedIn = false;
  FirebaseUser currentUser;

  bool _isGoogleAuthEnable = false;
  bool _isEmailAuthEnable = true;
  bool _isLoading = false;

  final _controllerEmail = TextEditingController();
  final _controllerPassword = TextEditingController();

  FocusNode myFocusNode;

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();

    isSignedIn();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  void addDevices(String email, String token) async {
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://skripsinotifyme.000webhostapp.com";

    FormData formData = new FormData.from({"email": email, "token": token});

    response = await dio.post("/RegisterDevice.php",
        data: formData, options: Options(method: "POST"));
    debugPrint("response dio: " + response.data.toString());
  }

  void isSignedIn() async {
    this.setState(() {
      isLoading = true;
    });

    prefs = await SharedPreferences.getInstance();

    isLoggedIn = await googleSignIn.isSignedIn();
    if (isLoggedIn) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                HomePage(currentUserId: prefs.getString('id'))),
      );
    }

    this.setState(() {
      isLoading = false;
    });
  }

  Future<Null> handleSignInGoogle() async {
    _isLoading = false;
    prefs = await SharedPreferences.getInstance();

    this.setState(() {
      isLoading = true;
    });

    GoogleSignIn _googleSignIn = new GoogleSignIn();

    GoogleSignInAccount googleUser =
    await _googleSignIn.signIn().catchError((e) => loginError(e));
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);

    FirebaseUser firebaseUser =
    await firebaseAuth.signInWithCredential(credential);

    if (firebaseUser != null) {
      final QuerySnapshot result = await Firestore.instance
          .collection('users')
          .where('id', isEqualTo: firebaseUser.uid)
          .getDocuments();
      final List<DocumentSnapshot> documents = result.documents;

      Fluttertoast.showToast(msg: "Sign in success");
      this.setState(() {
        isLoading = false;
      });

      Future<dynamic> firebaseMessaging =
      FirebaseMessaging().getToken().then((token) async {
        debugPrint('token: ' + token);
        addDevices(firebaseUser.email, token);

        if (documents.length == 0) {
          Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .setData({
            'email': firebaseUser.email,
            'nickname': firebaseUser.displayName,
            'photoUrl': firebaseUser.photoUrl,
            'id': firebaseUser.uid,
            'token': token
          });

          currentUser = firebaseUser;
          await prefs.setString('id', firebaseUser.uid);
          await prefs.setString('email', firebaseUser.email);
          await prefs.setString('nickname', firebaseUser.displayName);
          await prefs.setString('photoUrl', firebaseUser.photoUrl);
          await prefs.setString('token', token);
          await prefs.setString('loginType', '2');
        } else {
          Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .updateData({'token': token});
          await prefs.setString('id', documents[0]['id']);
          await prefs.setString('email', documents[0]['email']);
          await prefs.setString('nickname', documents[0]['nickname']);
          await prefs.setString('photoUrl', documents[0]['photoUrl']);
          await prefs.setString('aboutMe', documents[0]['aboutMe']);
          await prefs.setString('token', token);
          await prefs.setString('loginType', '2');
        }
      }).catchError((e) => loginError(e));

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
              currentUserId: firebaseUser.uid,
            )),
      );
    } else {
      Fluttertoast.showToast(msg: "Sign in fail");
      this.setState(() {
        isLoading = false;
      });
    }
  }


  Future<Null> handleSignInAnon(String email, String pass) async {
    FirebaseUser firebaseUser = await firebaseAuth.signInWithEmailAndPassword(
        email: _controllerEmail.text, password: _controllerPassword.text);
    Future<dynamic> firebaseMessaging =
    FirebaseMessaging().getToken().then((token) async {
      token = token;
      addDevices(firebaseUser.email, token);
      debugPrint('token: ' + token);
      if (firebaseUser != null) {
        final QuerySnapshot result = await Firestore.instance
            .collection('users')
            .where('id', isEqualTo: firebaseUser.uid)
            .getDocuments();
        final List<DocumentSnapshot> documents = result.documents;
        if (documents.length == 0) {
          Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .setData({
            'email': firebaseUser.email,
            'nickname': firebaseUser.displayName,
            'photoUrl': firebaseUser.photoUrl,
            'id': firebaseUser.uid,
            'token': FirebaseMessaging().getToken()
          });

          currentUser = firebaseUser;
          await prefs.setString('id', currentUser.uid);
          await prefs.setString('email', currentUser.email);
          await prefs.setString('nickname', currentUser.displayName);
          await prefs.setString('photoUrl', currentUser.photoUrl);
          await prefs.setString(
              'token', FirebaseMessaging().getToken().toString());
          await prefs.setString('loginType', '1');
        } else {
          await prefs.setString('id', documents[0]['id']);
          await prefs.setString('email', documents[0]['email']);
          await prefs.setString('nickname', documents[0]['nickname']);
          await prefs.setString('photoUrl', documents[0]['photoUrl']);
          await prefs.setString('aboutMe', documents[0]['aboutMe']);
          await prefs.setString(
              'token', FirebaseMessaging().getToken().toString());
          await prefs.setString('loginType', '1');

        }
        Fluttertoast.showToast(msg: "Sign in success");
        this.setState(() {
          isLoading = false;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(
                currentUserId: firebaseUser.uid,
              )),
        );
      } else {
        loginError("Sign in fail");
      }
    }).catchError((e) => loginError(e));
  }

  loginError(e) {
    setState(() {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(e),
      ));
      _isLoading = false;
    });
  }

  @override
  void onLoginError(String errorTxt) {
    setState(() => _isLoading = false);
  }

  @override
  void closeLoader() {
    setState(() => _isLoading = false);
  }

  @override
  void showAlert(String msg) {
    setState(() {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(msg),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.35,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    ClipPath(
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.35,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.red[500],
                      ),
                      clipper: RoundedClipper(60),
                    ),
                    ClipPath(
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.33,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.redAccent,
                      ),
                      clipper: RoundedClipper(50),
                    ),
                    Positioned(
                        top: -110,
                        left: -110,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.30,
                          width: MediaQuery.of(context).size.height * 0.30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  (MediaQuery.of(context).size.height * 0.30) / 2),
                              color: Colors.red[500].withOpacity(0.3)),
                          child: Center(
                            child: Container(
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.redAccent),
                            ),
                          ),
                        )),
                    Positioned(
                        top: -100,
                        left: 100,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.36,
                          width: MediaQuery.of(context).size.height * 0.36,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  (MediaQuery.of(context).size.height * 0.36) / 2),
                              color: Colors.red[500].withOpacity(0.3)),
                          child: Center(
                            child: Container(
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.redAccent),
                            ),
                          ),
                        )),
                    Positioned(
                        top: -50,
                        left: 60,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: MediaQuery.of(context).size.height * 0.15,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  (MediaQuery.of(context).size.height * 0.15) / 2),
                              color: Colors.red[500].withOpacity(0.3)),
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.15 - 50),
                      height: MediaQuery.of(context).size.height * 0.33,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            "assets/image/login.png",
                            height: MediaQuery.of(context).size.height * 0.15,
                            width: MediaQuery.of(context).size.height * 0.15,
                            fit: BoxFit.cover,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Notify Me",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.65,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 12, 20, 10),
                  child: Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: _controllerEmail,
                          validator: validateEmail,
                          onSaved: (String val) {
                            _email = val;
                          },
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: 16, color: Colors.black),
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            labelText: "Email",
                            contentPadding: new EdgeInsets.symmetric(
                                vertical:
                                MediaQuery.of(context).size.height * 0.022,
                                horizontal: 15.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(25)),
                            ),
                          ),
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(myFocusNode);
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerPassword,
                          validator: validatePassword,
                          onSaved: (String val) {
                            _password = val;
                          },
                          focusNode: myFocusNode,
                          obscureText: true,
                          keyboardType: TextInputType.text,
                          style: TextStyle(fontSize: 16, color: Colors.black),
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                            labelText: "Password",
                            contentPadding: new EdgeInsets.symmetric(
                                vertical:
                                MediaQuery.of(context).size.height * 0.022,
                                horizontal: 15.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(25)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Container(
                          child: GestureDetector(
                              onTap: () {
                                handleSignInAnon(_controllerEmail.text, _controllerPassword.text);
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.065,
                                decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(25))),
                                child: Center(
                                  child: Text(
                                    "SIGN IN",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                  ),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Connect with",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: (){
                                      Fluttertoast.showToast(msg: 'sorry this service is not available for now, please use google for other sign in !!');
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 7),
                                      height: MediaQuery.of(context).size.height *
                                          0.065,
                                      width: MediaQuery.of(context).size.height *
                                          0.065,
                                      decoration: BoxDecoration(
                                          color: Colors.blue[900],
                                          shape: BoxShape.circle),
                                      child: Center(
                                        child: Image.asset(
                                          "assets/image/facebook.png",
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: (){
                                      handleSignInGoogle();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 7),
                                      height:
                                      MediaQuery.of(context).size.height * 0.065,
                                      width:
                                      MediaQuery.of(context).size.height * 0.065,
                                      decoration: BoxDecoration(
                                          color: Colors.red, shape: BoxShape.circle),
                                      child: Image.asset(
                                        "assets/image/google.png",
                                        color: Colors.white,
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 10, bottom: 15),
                            height: MediaQuery.of(context).size.height * 0.05,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => SignupScreen()));
                              },
                              child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "New User?",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      "Signup",
                                      style: TextStyle(
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
          Positioned(
            child: isLoading
                ? Container(
              child: Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
              ),
              color: Colors.white.withOpacity(0.8),
            )
                : Container(),
          ),
        ],
      )
    );
  }

  bool _value1 = false;
  bool _autoValidate = false;

  void _value1Changed(bool value) => setState(() => _value1 = value);

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.length < 6)
      return 'Password must be atleast 6 digits';
    else
      return null;
  }
}

class RoundedClipper extends CustomClipper<Path> {
  var differenceInHeights = 0;

  RoundedClipper(int differenceInHeights) {
    this.differenceInHeights = differenceInHeights;
  }

  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - differenceInHeights);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
