import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'const.dart';

class AddNotification extends StatefulWidget {
  final String idGroup;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  const AddNotification({Key key, this.idGroup, this.currentUserId, this.memberGroup, this.groupName}) : super(key: key);

  @override
  _AddNotificationState createState() =>
      _AddNotificationState(idGroup: idGroup, currentUserId: currentUserId, memberGroup: memberGroup, groupName: groupName);
}

class _AddNotificationState extends State<AddNotification> {
  final String idGroup;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  _AddNotificationState({@required this.idGroup, @required this.currentUserId, @required this.memberGroup, @required this.groupName});

  List notifType = ["Invitation", "Reminder"];
  List notifScheduling = ["Now", "Scheduled"];
  List<DropdownMenuItem<String>> _dropDownType;
  List<DropdownMenuItem<String>> _dropDownSchedule;

  String _currentNotifType, _currentNotifScheduling;

  TextEditingController controllerTitle = new TextEditingController();
  TextEditingController controllerMessage = new TextEditingController();
  TextEditingController controllerDate = new TextEditingController();
  TextEditingController controllerTime = new TextEditingController();

  List<DropdownMenuItem<String>> getDropDownType() {
    List<DropdownMenuItem<String>> items = new List();
    for (String type in notifType) {
      items.add(new DropdownMenuItem(
          value: type,
          child: new Text(type)
      ));
    }
    return items;
  }
  List<DropdownMenuItem<String>> getDropDownSchedule() {
    List<DropdownMenuItem<String>> items = new List();
    for (String schedule in notifScheduling) {
      items.add(new DropdownMenuItem(
          value: schedule,
          child: new Text(schedule)
      ));
    }
    return items;
  }

  void changedDropDownType(String selectedType) {
    setState(() {
      _currentNotifType = selectedType;
    });
  }

  void changedDropDownSchedule(String selectedSchedule) {
    setState(() {
      _currentNotifScheduling = selectedSchedule;
    });
  }

  int _radioValue = 0;
  String notificationType ='';

  bool isLoading = false;

  final dateFormat = new DateFormat('dd-MM-yyyy');
  final timeFormat = new DateFormat('HH');
  DateTime _date = new DateTime.now();
  TimeOfDay _time = new TimeOfDay.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2019),
        lastDate: new DateTime(2025)
    );

    if (picked != null && picked != _date) {
      print('Date selected: ${_date.toString()}');
      setState(() {
        _date = picked;
        controllerDate.text = dateFormat.format(_date).toString();
      });
    }
  }

  Future<Null> _selectTime(BuildContext context) async {

    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: _time,
        builder: (BuildContext context, Widget child){
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child,
          );
        }
    );

    if (picked != null && picked != _time) {
      print('Date selected: ${picked.hour.toString()}');
      setState(() {
        _time = picked;
        controllerTime.text = picked.format(context);
      });
    }
  }

  @override
  void initState() {
    _dropDownType = getDropDownType();
    _dropDownSchedule =getDropDownSchedule();
    _currentNotifType = _dropDownType[0].value;
    _currentNotifScheduling =_dropDownSchedule[0].value;
    
    controllerDate.text = dateFormat.format(_date);
    controllerTime.text = "${_time.hour}:${_time.minute}";
  }


  void pushNotification() async {
    setState(() {
      isLoading = true;
    });
    try{
      final result = await InternetAddress.lookup('google.com');
      if (_currentNotifScheduling == "Now"){
        Response response;
        var dio = Dio();
        dio.options.baseUrl = "http://skripsinotifyme.000webhostapp.com";
        FormData formData = new FormData.from({"idgroup": idGroup, "title": groupName+": "+controllerTitle.text, "message": controllerMessage.text});
        String notId;
        try{
          response = await dio.post("/sendMultiplePush.php",
              data: formData, options: Options(method: "POST"));
          debugPrint("response dio: " + response.data.toString());
          debugPrint("time sent: "+DateTime.now().toString());
          final collRef = Firestore.instance.collection('notification');
          DocumentReference docReferance = collRef.document();
          setState(() {
            isLoading = true;
            docReferance.setData({
              'groupId': idGroup,
              'groupName': groupName,
              'adminId': currentUserId,
              'title': controllerTitle.text,
              'message': controllerMessage.text,
              'date': controllerDate.text,
              'time': controllerTime.text,
              'attendance': FieldValue.arrayUnion(['first commit']),
              'status': 'Sent',
              'notificationId': docReferance.documentID,
              'sendTo': FieldValue.arrayUnion(memberGroup),
              'notificationType': _currentNotifType,
              'notificationScheduling': _currentNotifScheduling,
              'timestamp': DateTime.now().millisecondsSinceEpoch.toString()
            }).then((data){
              setState(() {
                notId = docReferance.documentID;
                isLoading = false;
                Navigator.pop(context);
              });
            }).catchError((err){
              setState(() {
                isLoading = false;
              });
              Fluttertoast.showToast(msg: err);
            });
          });
        } catch(e){
          debugPrint('fail to push notification');
          Fluttertoast.showToast(msg: e.toString());
        }
      } else {
        final collRef = Firestore.instance.collection('notification');
        DocumentReference docReferance = collRef.document();
        setState(() {
          isLoading = true;
          docReferance.setData({
            'groupId': idGroup,
            'groupName': groupName,
            'adminId': currentUserId,
            'title': controllerTitle.text,
            'message': controllerMessage.text,
            'date': controllerDate.text,
            'time': controllerTime.text,
            'attendance': FieldValue.arrayUnion(['first commit']),
            'status': 'Waiting',
            'notificationId': docReferance.documentID,
            'sendTo': FieldValue.arrayUnion(memberGroup),
            'notificationType': _currentNotifType,
            'notificationScheduling': _currentNotifScheduling
          }).then((data){
            setState(() {
              isLoading = false;
              Navigator.pop(context);
            });
          }).catchError((err){
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: err);
          });
        });
      }
    }on SocketException catch (_){
      Fluttertoast.showToast(msg: 'Push Failed! Make sure your internet connection is running!');
    }
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Add Notification"),
          backgroundColor: Colors.redAccent,
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  // Input
                  Column(
                    children: <Widget>[
                      // Username
                      // new Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: <Widget>[
                      //     new Radio(
                      //       value: 1,
                      //       groupValue: _radioValue,
                      //       onChanged: _handleRadioValueChange,
                      //     ),
                      //     new Text(
                      //       'Undangan',
                      //       style: new TextStyle(fontSize: 16.0),
                      //     ),
                      //     new Radio(
                      //       value: 2,
                      //       groupValue: _radioValue,
                      //       onChanged: _handleRadioValueChange,
                      //     ),
                      //     new Text(
                      //       'Pengingat',
                      //       style: new TextStyle(
                      //         fontSize: 16.0,
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      Container(
                        child: Text(
                          'Notification Type',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      Container(
                        child: new DropdownButton(
                          value: _currentNotifType,
                          items: _dropDownType,
                          onChanged: changedDropDownType,
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                      Container(
                        child: Text(
                          'Title',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      Container(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              primaryColor: primaryColor),
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'Title',
                              contentPadding: new EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: greyColor),
                            ),
                            controller: controllerTitle,
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                      Container(
                        child: Text(
                          'Message',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      Container(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              primaryColor: primaryColor),
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'Message',
                              contentPadding: new EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: greyColor),
                            ),
                            maxLines: 5,
                            keyboardType: TextInputType.multiline,
                            controller: controllerMessage,
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                      Container(
                        child: Text(
                          'Scheduling',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      Container(
                        child: new DropdownButton(
                          value: _currentNotifScheduling,
                          items: _dropDownSchedule,
                          onChanged: changedDropDownSchedule,
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                      (_currentNotifScheduling == "Now") ? Container() :
                      Container(
                        child: Text(
                          'Date',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      (_currentNotifScheduling == "Now") ? Container() :
                      Container(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              primaryColor: primaryColor),
                          child: TextField(
                            decoration: InputDecoration(
                              icon: Icon(Icons.date_range),
                              hintText: 'Date',
                              contentPadding: new EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: greyColor),
                            ),
                            controller: controllerDate,
                            onTap: (){
                              _selectDate(context);
                            },
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                      (_currentNotifScheduling == "Now") ? Container() :
                      Container(
                        child: Text(
                          'Time',
                          style: TextStyle(fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        margin: EdgeInsets.only(
                            left: 10.0, bottom: 5.0, top: 10.0),
                      ),
                      (_currentNotifScheduling == "Now") ? Container() :
                      Container(
                        child: Theme(
                          data: Theme.of(context).copyWith(
                              primaryColor: primaryColor),
                          child: TextField(
                            decoration: InputDecoration(
                              icon: Icon(Icons.access_time),
                              hintText: 'Time',
                              contentPadding: new EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: greyColor),
                            ),
                            controller: controllerTime,
                            onTap: (){
                              _selectTime(context);
                            },
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),

                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),

                  // Button
                  Container(
                    child: FlatButton(
                      onPressed: () {
                        pushNotification();
                      },
                      child: Text(
                        'PUSH',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      color: primaryColor,
                      highlightColor: new Color(0xff8d93a0),
                      splashColor: Colors.transparent,
                      textColor: Colors.white,
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                    ),
                    margin: EdgeInsets.only(top: 50.0),
                  ),
                ],
              ),
              padding: EdgeInsets.only(left: 15.0, right: 15.0),
            ),

            // Loading
            Positioned(
              child: isLoading
                  ? Container(
                child: Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
                ),
                color: Colors.white.withOpacity(0.8),
              )
                  : Container(),
            ),
          ],
        )
    );
  }
}
