import 'package:notifyme/add_notification.dart';
import 'package:notifyme/attendance_list.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'const.dart';

class GroupNotificationAttendance extends StatefulWidget {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  GroupNotificationAttendance({Key key, this.groupId, this.currentUserId, this.memberGroup, this.groupName})
      : super(key: key);

  @override
  _GroupNotificationAttendanceState createState() =>
      _GroupNotificationAttendanceState(groupId: groupId, currentUserId: currentUserId, memberGroup: memberGroup, groupName: groupName);
}

class _GroupNotificationAttendanceState extends State<GroupNotificationAttendance> {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final List<String> memberGroup;

  _GroupNotificationAttendanceState(
      {@required this.groupId, @required this.currentUserId, @required this.memberGroup, @required this.groupName});

  bool isLoading = false;

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
//    GroupModel groupModel = new GroupModel();
//    groupModel.setId = document['idGroup'];
//    groupModel.setName = document['nameGroup'];
//    groupModel.setPhotoUrl = document['photoUrl'];
//    List<String> members = List.from(document['members']);
//    dataList.add(groupModel);

    if (document['status'] == 'Sent'){
      return Container(
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Card(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Center(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => AttendanceList(groupName: groupName,currentUserId: currentUserId,memberGroup: memberGroup,groupId: groupId,notificationId: document['notificationId'],)));
                    },
                    leading: CircleAvatar(
                      child: Text(
                        document['title'][0].toString().toUpperCase(),
                        style: TextStyle(
                            fontSize: 24.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    title: Text(
                      document['title'].toString(),
                      style: TextStyle(),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Text("Push in " +
                          document['date'] +
                          " at " +
                          document['time'] +
                          " o'clock"),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Event List"),
        backgroundColor: Colors.redAccent,
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream:
                Firestore.instance.collection('notification').where('groupId', isEqualTo: groupId).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) =>
                          buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading
                  ? Container(
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  ),
                ),
                color: Colors.white.withOpacity(0.8),
              )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}
