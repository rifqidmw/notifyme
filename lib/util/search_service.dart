import 'package:cloud_firestore/cloud_firestore.dart';

class SearchService {
  searchByEmail(String searchField) {
    return Firestore.instance
        .collection('users')
        .where('email', isGreaterThanOrEqualTo: searchField)
        .snapshots();
  }
}