import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifyme/add_notification.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'const.dart';

class AttendanceList extends StatefulWidget {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final String notificationId;
  final List<String> memberGroup;

  AttendanceList({Key key, this.groupId, this.currentUserId, this.memberGroup, this.groupName, this.notificationId})
      : super(key: key);

  @override
  _AttendanceListState createState() =>
      _AttendanceListState(groupId: groupId, currentUserId: currentUserId, memberGroup: memberGroup, groupName: groupName, notificationId: notificationId);
}

class _AttendanceListState extends State<AttendanceList> {
  final String groupId;
  final String currentUserId;
  final String groupName;
  final String notificationId;
  final List<String> memberGroup;

  _AttendanceListState(
      {@required this.groupId, @required this.currentUserId, @required this.memberGroup, @required this.groupName, @required this.notificationId});

  bool isLoading = false;

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.all(2.0),
      child: Card(
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        elevation: 5.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Center(
              child: ListTile(
                onTap: () {

                },
                leading: CircleAvatar(
                  child: Material(
                    child: CachedNetworkImage(
                      imageUrl: document['idAvatar'],
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    clipBehavior: Clip.hardEdge,
                  ),
                ),
                title: Text(
                  document['name'].toString(),
                  style: TextStyle(),
                ),
                subtitle: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: (document['attendanceType'] == 'Hadir')
                        ? Text('Status: Hadir')
                        : Text(document['message'])
                ),
                trailing: IconButton(
                  color: Colors.redAccent,
                  icon: (document['attendanceType'] == 'Hadir'
                      ? Icon(Icons.check)
                      : Icon(Icons.close)),
                  onPressed: () {},
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Attendance List"),
        backgroundColor: Colors.redAccent,
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream:
                Firestore.instance.collection('notification').document(notificationId).collection('attendance').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) =>
                          buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading
                  ? Container(
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  ),
                ),
                color: Colors.white.withOpacity(0.8),
              )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}
