import 'package:flutter/widgets.dart';

class GKey {
  static final gKeyTimeline = const Key('__GKEYTIMELINE__');
  static final gKeyLogin = const Key('__GKEYSIGNIN__');
  static final gKeySignup = const Key('__GKEYSIGNUP__');
}