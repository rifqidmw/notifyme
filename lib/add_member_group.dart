import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifyme/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'const.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'model/friend_model.dart';

class AddMemberGroup extends StatelessWidget {
  final String currentUserId;
  final String currentUserEmail;
  final String currentGroupId;
  final List<String> memberGroup;

  const AddMemberGroup({Key key, this.currentUserId, this.currentGroupId, this.currentUserEmail, this.memberGroup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Invite to Group"),
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.check),
              onPressed: () {

                Navigator.pop(context);
              }

//            Navigator.pop(context),
          ),
        ],
      ),
      body: new AddMemberGroupScreen(
        currentUserId: currentUserId,
        currentGroupId: currentGroupId,
        currentUserEmail: currentUserEmail,
        memberGroup: memberGroup,
      ),
    );
  }
}

class AddMemberGroupScreen extends StatefulWidget {
  final String currentUserId;
  final String currentUserEmail;
  final String currentGroupId;
  final List<String> memberGroup;

  const AddMemberGroupScreen({Key key, this.currentUserId, this.currentGroupId, this.currentUserEmail, this.memberGroup}) : super(key: key);

  @override
  State createState() => new AddMemberGroupState(currentUserId: currentUserId, currentGroupId: currentGroupId, currentUserEmail: currentUserEmail, memberGroup: memberGroup,);
}

class AddMemberGroupState extends State<AddMemberGroupScreen> {
  final String currentUserId;
  final String currentUserEmail;
  final String currentGroupId;
  final List<String> memberGroup;

  bool isLoading = false;
  List<FriendModel> dataList = new List();
  List<FriendModel> backDataList = new List();
  String dataSearch = '';

  List<String> selectedEmail = new List();


  AddMemberGroupState({Key key, @required this.currentUserId, @required this.currentGroupId, @required this.currentUserEmail, @required this.memberGroup});

  @override
  void initState() {
    super.initState();
  }

  TextEditingController editingController = TextEditingController();

  Widget buildSelectedItem(BuildContext context, DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    friendModel.setToken = document['token'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
    return Container(
      child: FlatButton(
        child: Column(
          children: <Widget>[
            Material(
              child: CachedNetworkImage(
                // placeholder: Container(
                //   child: CircularProgressIndicator(
                //     strokeWidth: 1.0,
                //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                //   ),
                //   width: 50.0,
                //   height: 50.0,
                //   padding: EdgeInsets.all(15.0),
                // ),

                imageUrl: friendModel.get_photoUrl,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
              clipBehavior: Clip.hardEdge,
            ),
            new Expanded(
              child: Container(
                child: new Center(
                  child: new Container(
                    child: Text(
                      '${friendModel.get_nickname}',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: primaryColor),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
              ),
            ),
          ],
        ),
        onPressed: () {
          _deleteSelectedItem(friendModel.get_nickname, friendModel.get_email,
              friendModel.get_photoUrl, friendModel.get_id, friendModel.get_token);

          setState(() {
            selectedEmail.remove(friendModel.get_email);
          });
        },
//        color: greyColor2,
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
//        shape:
//              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      margin: EdgeInsets.only(bottom: 5.0, left: 5.0, right: 5.0, top: 5.0),
    );
  }

  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document, int index) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    friendModel.setToken = document['token'];
    dataList.add(friendModel);
    backDataList.add(friendModel);


    if (memberGroup.contains(friendModel.get_id)) {
      return Container();
    } else {
      if (friendModel.get_email.contains(dataSearch)){
        return Container(
          child: FlatButton(
            child: Row(
              children: <Widget>[
                Material(
                  child: CachedNetworkImage(
                    // placeholder: Container(
                    //   child: CircularProgressIndicator(
                    //     strokeWidth: 1.0,
                    //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                    //   ),
                    //   width: 50.0,
                    //   height: 50.0,
                    //   padding: EdgeInsets.all(15.0),
                    // ),

                    imageUrl: friendModel.get_photoUrl,
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  clipBehavior: Clip.hardEdge,
                ),
                new Flexible(
                  child: Container(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          child: Text(
                            'Nickname: ${friendModel.get_nickname}',
                            style: TextStyle(color: primaryColor),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                        ),
                        new Container(
                          child: Text(
                            '${friendModel.get_email ?? 'Not available'}',
                            style: TextStyle(color: primaryColor),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 20.0),
                  ),
                ),
                (selectedEmail.contains(friendModel.get_email))?
                IconButton(
                  icon: IconButton(
                      icon: Icon(Icons.check),
                      color: Colors.redAccent,
                      onPressed: (){

                      }),
                ) : Container()
              ],
            ),
            onPressed: () {
              setState(() {
                if (!selectedEmail.contains(document['email'])){
                  selectedEmail.add(friendModel.get_email);
                  _selectedItem(friendModel.get_nickname, friendModel.get_email,
                      friendModel.get_photoUrl, friendModel.get_id, friendModel.get_token);
                }
              });

            },
            color: greyColor2,
            padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          ),
          margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
        );
      } else {
        return Container();
      }
    }
  }

  void filterSearchResults(String query) {
    List<FriendModel> dummySearchList = List<FriendModel>();
    dummySearchList.addAll(dataList);
    if(query.isNotEmpty) {
      List<FriendModel> dummyListData = List<FriendModel>();
      dummySearchList.forEach((item) {
        if(dataList.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        dataList.clear();
        dataList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        dataList.clear();
        dataList.addAll(backDataList);
      });
    }

  }

  void _selectedItem(String nickname, String email, String photoUrl, String id, String token) {
    final collRef = Firestore.instance.collection('groups');
    DocumentReference docReferance = collRef.document(currentGroupId);
    docReferance.updateData({'members': FieldValue.arrayUnion([id])});
    Firestore.instance
        .collection('groups')
        .document(currentGroupId)
        .collection('members')
        .document(id)
        .setData({
      'email': email,
      'nickname': nickname,
      'photoUrl': photoUrl,
      'id': id
    });

    addGroup(currentGroupId, token, email);
  }

  void _deleteSelectedItem(String nickname, String email, String photoUrl, String id, String token) {
    Firestore.instance
        .collection('groups')
        .document(currentGroupId)
        .collection('members')
        .document(id)
        .delete();

    deleteFromGroup(currentGroupId, email);
  }

  void addGroup(String idGroup, String token, String email) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://skripsinotifyme.000webhostapp.com";

    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "token": token,
      "email": email
    });

    response = await dio.post("/RegisterGroup.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }
  void deleteFromGroup(String idGroup, String email) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://skripsinotifyme.000webhostapp.com";
    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "email": email
    });

    response = await dio.post("/deleteFromGroup.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        StreamBuilder(
          stream: Firestore.instance.collection('groups').document(currentGroupId).collection('members').snapshots(),
          builder: (context, snapshot){
            debugPrint('user id: $currentUserId');
            debugPrint('group id in two: $currentGroupId');
            if (!snapshot.hasData){
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                ),
              );
            } else {
              return GridView.builder(
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                shrinkWrap: true,
                itemBuilder: (context, index) => buildSelectedItem(context, snapshot.data.documents[index]),
                itemCount: snapshot.data.documents.length,
              );
            }
          },
        ),

        Padding(
          padding: const EdgeInsets.all(10.0),
          child: TextField(
            onChanged: (value) {
              filterSearchResults(value);
              dataSearch = value;
            },
            controller: editingController,
            decoration: InputDecoration(
                labelText: "Search by Email",
                hintText: "ex: example@email.com",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)))),
          ),
        ),
        StreamBuilder(
          stream: Firestore.instance.collection('users').snapshots(),
          builder: (context, snapshot){
            if (!snapshot.hasData){
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                ),
              );
            } else{
              return ListView.builder(
                padding: EdgeInsets.all(10.0),
                itemBuilder: (context, index) =>
                    buildItem(context, snapshot.data.documents[index], index)
                ,
                itemCount: snapshot.data.documents.length,
                shrinkWrap: true,
              );
            }
          },
        ),
      ],
    );
  }
}
