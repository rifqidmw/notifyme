import 'package:notifyme/GKey.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'const.dart';
import 'package:flutter_progress_button/flutter_progress_button.dart';
import 'dart:math' as math show sin, pi;

class TimelineScreen extends StatefulWidget {
  final String currentUserId;

  const TimelineScreen({Key key, this.currentUserId}) : super(key: key);

  @override
  _TimelineScreenState createState() =>
      _TimelineScreenState(currentUserId: currentUserId);
}

class _TimelineScreenState extends State<TimelineScreen> {
  final String currentUserId;
  bool isLoading = false;

  _TimelineScreenState({@required this.currentUserId});

  SharedPreferences prefs;

  TextEditingController _controller;

  String id = '';
  String nickname = '';
  String aboutMe = '';
  String photoUrl = '';

  @override
  void initState() {
    super.initState();
    setState(() {
      readLocal();
    });
  }

  void readLocal() async {
    prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id') ?? '';
    nickname = prefs.getString('nickname') ?? '';
    aboutMe = prefs.getString('aboutMe') ?? '';
    photoUrl = prefs.getString('photoUrl') ?? '';
  }

  void onSendMessage(String content, int type, String attendanceType,
      String groupId, String notificationId) {
    if (content.trim() != '') {
      Firestore.instance
          .collection('messages')
          .document(groupId)
          .collection(groupId)
          .document(DateTime.now().millisecondsSinceEpoch.toString())
          .setData({
        'idFrom': id,
        'idTo': groupId,
        'idAvatar': photoUrl,
        'nameFrom': nickname,
        'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
        'content': content,
        'type': type
      });

      Firestore.instance
          .collection('notification')
          .document(notificationId)
          .collection('attendance')
          .document(id)
          .setData({
        'id': id,
        'idAvatar': photoUrl,
        'name': nickname,
        'message': content,
        'attendanceType': attendanceType
      });

      final collRef = Firestore.instance.collection('notification');
      DocumentReference docReferance = collRef.document(notificationId);
      docReferance.updateData({
        'attendance': FieldValue.arrayUnion([id])
      });
    } else {
      Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    List<String> members = List.from(document['sendTo']);
    List<String> attendance = List.from(document['attendance']);

    if (members.contains(currentUserId)) {
      if (document['status'] == 'Sent') {
        return Column(
          children: <Widget>[
            Container(
              child: Text(
                DateFormat('dd MMM kk:mm').format(
                    DateTime.fromMillisecondsSinceEpoch(
                        int.parse(document['timestamp']))),
                style: TextStyle(
                    color: greyColor,
                    fontSize: 12.0,
                    fontStyle: FontStyle.italic),
              ),
              margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
            ),
            Card(
                margin: EdgeInsets.only(left: 4.0, right: 4.0, bottom: 16.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 4.0,
                child: new Column(children: <Widget>[
                  new Container(
                    padding: new EdgeInsets.only(
                        left: 16.0, right: 16.0, bottom: 8.0),
                    child: new InkWell(
                      child: new Container(
                        child: new Column(
                          children: <Widget>[
                            new Container(height: 16.0),
                            new Wrap(
                              // gap between adjacent chips
                              spacing: 4.0,
                              // gap between lines
                              runSpacing: 4.0,
                              // main axis (rows or columns)
                              direction: Axis.horizontal,
                              children: <Widget>[

                                new Container(
                                    child: new Text('From: '+document['groupName'],
                                        style: new TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontFamily: 'Raleway',
                                          fontSize: 14.0,
                                        ),
                                        textAlign: TextAlign.left)),
                                new Container(
                                  height: 5.0,
                                ),
                                new Container(
                                    child: new Text(document['notificationType']+": "+document['title'],
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Raleway',
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.left)),
                                new Container(
                                  height: 16.0,
                                ),
                                new Container(
                                    child: new Text(document["message"],
                                        style: TextStyle(
                                            fontFamily: 'Raleway',
                                            fontSize: 14.0))),
                                new Divider(
                                  height: 20.0,
                                  color: Colors.blue,
                                ),
                              ],
                            ),
                            (!attendance.contains(id) && document['notificationType'] == 'Invitation')
                                ? new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      ProgressButton(
                                        defaultWidget: const Text(
                                          'Attend',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        progressWidget: ThreeSizeDot(
                                          color_1: Colors.black54,
                                          color_2: Colors.black54,
                                          color_3: Colors.black54,
                                        ),
                                        width: 130,
                                        color: Colors.greenAccent,
                                        height: 40,
                                        borderRadius: 24,
                                        animate: false,
                                        onPressed: () async {
                                          int score = await Future.delayed(
                                              const Duration(
                                                  milliseconds: 3000),
                                              () => 42);
                                          return () {
                                            String content =
                                                "Saya dapat menghadiri " +
                                                    document['title'];
                                            onSendMessage(
                                                content,
                                                0,
                                                'Hadir',
                                                document['groupId'],
                                                document['notificationId']);
                                          };
                                        },
                                      ),
                                      SizedBox(
                                        width: 7,
                                      ),
                                      ProgressButton(
                                        defaultWidget: const Text(
                                          'Cant Attend',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        progressWidget: ThreeSizeDot(
                                          color_1: Colors.black54,
                                          color_2: Colors.black54,
                                          color_3: Colors.black54,
                                        ),
                                        width: 130,
                                        height: 40,
                                        color: Colors.redAccent,
                                        borderRadius: 24,
                                        animate: false,
                                        onPressed: () async {
                                          String alasan = '';
                                          int wait = 1;
                                          showDialog(
                                            barrierDismissible: false,
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: new Text(
                                                      'Reason for Absence'),
                                                  content: TextField(
                                                    controller: _controller,
                                                    onChanged: (value) {
                                                      alasan = value;
                                                    },
                                                    decoration: InputDecoration(
                                                        hintText: "Reason"),
                                                  ),
                                                  actions: <Widget>[
                                                    new FlatButton(
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child:
                                                            new Text('Close')),
                                                    new FlatButton(
                                                        onPressed: () {
                                                          debugPrint('alasan: $alasan');
                                                          String content =
                                                              "Saya tidak dapat menghadiri " +
                                                                  document['title'] +
                                                                  ", dikarenakan: " +
                                                                  alasan;
                                                          onSendMessage(
                                                              content,
                                                              0,
                                                              'Tidak Hadir',
                                                              document['groupId'],
                                                              document['notificationId']);
                                                          Navigator.of(context)
                                                              .pop();

                                                        },
                                                        child:
                                                            new Text('Send')),
                                                  ],
                                                );
                                              });
                                          int score = await Future.delayed(
                                              const Duration(
                                                  milliseconds: 3000),
                                                  () => 42);

                                          return () {

                                          };
                                        },
                                      ),
                                    ],
                                  )
                                : new Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                ])),
          ],
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream:
                    Firestore.instance.collection('notification').orderBy('timestamp', descending: true).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) =>
                          buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading
                  ? Container(
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                        ),
                      ),
                      color: Colors.white.withOpacity(0.8),
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}

class ThreeSizeDot extends StatefulWidget {
  ThreeSizeDot(
      {Key key,
      this.shape = BoxShape.circle,
      this.duration = const Duration(milliseconds: 1000),
      this.size = 8.0,
      this.color_1,
      this.color_2,
      this.color_3,
      this.padding = const EdgeInsets.all(2)})
      : super(key: key);

  final BoxShape shape;
  final Duration duration;
  final double size;
  final Color color_1;
  final Color color_2;
  final Color color_3;
  final EdgeInsetsGeometry padding;

  @override
  _ThreeSizeDotState createState() => _ThreeSizeDotState();
}

class _ThreeSizeDotState extends State<ThreeSizeDot>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation_1;
  Animation<double> animation_2;
  Animation<double> animation_3;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: widget.duration);
    animation_1 = DelayTween(begin: 0.0, end: 1.0, delay: 0.0)
        .animate(animationController);
    animation_2 = DelayTween(begin: 0.0, end: 1.0, delay: 0.2)
        .animate(animationController);
    animation_3 = DelayTween(begin: 0.0, end: 1.0, delay: 0.4)
        .animate(animationController);
    animationController.repeat();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ScaleTransition(
            scale: animation_1,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color:
                    widget.color_1 ?? Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_2,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color:
                    widget.color_2 ?? Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_3,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color:
                    widget.color_3 ?? Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Dot extends StatelessWidget {
  final BoxShape shape;
  final double size;
  final Color color;

  Dot({
    Key key,
    this.shape,
    this.size,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(color: color, shape: shape),
      ),
    );
  }
}

class DelayTween extends Tween<double> {
  DelayTween({
    double begin,
    double end,
    this.delay,
  }) : super(begin: begin, end: end);

  final double delay;

  @override
  double lerp(double t) =>
      super.lerp((math.sin((t - delay) * 2 * math.pi) + 1) / 2);

  @override
  double evaluate(Animation<double> animation) => lerp(animation.value);
}
